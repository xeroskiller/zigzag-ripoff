﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject platformPrefab;
    public GameObject powerUpPrefab;
    public GameObject PlatformsContainer;
    public GameObject powerUpContainer;
    public GameObject player;
    public GameObject camera;
    public Text scoreText;
    [Range(1,100)]
    public int platformCount;
    public float chanceForBonus;
    public float ic;

    private Direction dir = Direction.x;
    private float currentX, currentZ;
    private Rigidbody rb;
    private int score;
    private int highScore;
    private Vector3 powerUpOffset;
    public static bool isShuttingDown;
    public bool lastPlatformOmitted;

	// Use this for initialization
	void Start () {
        currentX = 8.5f;
        currentZ = 6.5f;
        highScore = 0; // <TODO> : add permanence to high score
        powerUpOffset = new Vector3(0f, 1.1f, 0f);
        isShuttingDown = false;
        AddScore(0);

        for (int i = 0; i < platformCount; i++)
            AddPlatform();

        rb = GetComponent<Rigidbody>();
	}

    public void AddPlatform()
    {
        GameObject ptfrm = null;

        if (lastPlatformOmitted || Random.value > ic)
        {
            lastPlatformOmitted = false;
            ptfrm = (GameObject)SafeInstantiate(platformPrefab, new Vector3(currentX, -0.5f, currentZ), Quaternion.Euler(0f, 0f, 0f));
            if (ptfrm != null)
                ptfrm.transform.parent = PlatformsContainer.transform;
        }
        else
            lastPlatformOmitted = true;

        // Change direction
        if (Random.value > 0.5f)
            dir = (dir == Direction.z ? Direction.x : Direction.z);

        if (ptfrm != null && Random.value > 1f - chanceForBonus)
        {
            GameObject pu = (GameObject)SafeInstantiate(powerUpPrefab, ptfrm.transform.position + powerUpOffset, Random.rotation);
            if (pu != null)
                pu.transform.parent = powerUpContainer.transform;
        }

        // Increment locations
        if (dir == Direction.x)
            currentX += 2;
        else
            currentZ += 2;
    }

    public Object SafeInstantiate(GameObject obj, Vector3 pos, Quaternion rot)
    {
        if (isShuttingDown)
            return null;
        else
            return Instantiate(obj, pos, rot);
    }

    public void AddScore(int sc)
    {
        score += sc;
        scoreText.text = "Score: " + score.ToString();

        if (score % 500 == 0)
        {
            ic *= 1.5f;
            player.GetComponent<PlayerController>().speed += 1;
        }

        if (score > highScore)
            highScore = score;
    }

    void OnApplicationQuit()
    {
        isShuttingDown = true;
    }

    public enum Direction { x, z };
}