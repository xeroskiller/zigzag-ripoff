﻿using UnityEngine;
using System.Collections;

public class DestroyOnContact : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter(Collider col)
    {
        if (!col.gameObject.tag.Equals("Player"))
            Destroy(col.gameObject);
        else
        {
            GameController.isShuttingDown = true;
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}
