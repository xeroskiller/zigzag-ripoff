﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    [Range(0.1f,100f)]
    public float speed;
    public float timeBetweenPresses = 0.05f;
    public float jumpPower;

    private Direction direction = Direction.none;
    private Rigidbody rigidBody;
    private float lastPress = 0f;
    private Vector3 moveX, moveZ;
    private GameController gameController;
    private bool isJumping;

    void OnCollisionExit(Collision col)
    {
        if (col.rigidbody)
        {
            col.rigidbody.isKinematic = false;
            col.rigidbody.useGravity = true;
            gameController.AddScore(10);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        isJumping = false;
    }

	// Use this for initialization
	void Start () {
        rigidBody    = GetComponent<Rigidbody>();
        moveX = new Vector3(speed, 0f, 0f);
        moveZ = new Vector3(0f, 0f, speed);
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {

        bool movementFlag = Input.GetAxis("Fire1") > 0f && Time.time > lastPress + timeBetweenPresses;
        bool jumpFlag = Input.GetAxis("Fire2") > 0f && Time.time > lastPress + timeBetweenPresses;
        Vector3 vel = rigidBody.velocity;

        if (movementFlag)
        {
            lastPress = Time.time;
            if (direction == Direction.none || direction == Direction.x)
                direction = Direction.z;
            else if (direction == Direction.z)
                direction = Direction.x;
        }

        if (jumpFlag && !isJumping)
        {
            isJumping = true;
            vel.y = jumpPower;
        }

        vel.x = direction == Direction.x ? speed : 0f;
        vel.z = direction == Direction.z ? speed : 0f;
        rigidBody.velocity = vel;
	}

    public enum Direction { x, y, z, none };
}
