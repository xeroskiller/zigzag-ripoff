﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject player;

    private Vector3 offsetFromPlayer;

	// Use this for initialization
	void Start () {
        offsetFromPlayer = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (player.transform.position.y > -1f)
            transform.position = player.transform.position + offsetFromPlayer;
	}
}
