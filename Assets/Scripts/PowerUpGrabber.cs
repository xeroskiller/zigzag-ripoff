﻿using UnityEngine;
using System.Collections;

public class PowerUpGrabber : MonoBehaviour {

    public int value;

    private GameController gc;

	// Use this for initialization
	void Start () {
        gc = (GameController)GameObject.Find("GameController").GetComponent<GameController>();
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            Debug.Log("Score");
            Destroy(gameObject);
            gc.AddScore(value); 
        }
    }
}
