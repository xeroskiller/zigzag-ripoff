﻿using UnityEngine;
using System.Collections;

public class HoveringLightWaver : MonoBehaviour {

    public float lightSpeedX = 1.0f, lightSpeedY = 1.0f;
    public GameObject player;

    private Vector3 offsetFromPlayer;

	// Use this for initialization
	void Start () {
        offsetFromPlayer = gameObject.transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 deltaPosition = new Vector3(Mathf.Sin(lightSpeedX * Time.deltaTime), 0f, Mathf.Sin(lightSpeedY * Time.deltaTime));

        transform.position = offsetFromPlayer + deltaPosition;
	}
}
